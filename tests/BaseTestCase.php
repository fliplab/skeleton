<?php

namespace :uc:vendor\:uc:package\Tests;

use Orchestra\Testbench\TestCase;

abstract class BaseTestCase extends TestCase
{
    /**
     * Setup before each test
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
    }

    /**
     * Tear down after each test
     *
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * Get the package providers
     * @param  $app
     * @return array
     */
    protected function getPackageProviders($app)
    {
        return [':uc:vendor\:uc:package\:uc:packageServiceProvider'];
    }
}
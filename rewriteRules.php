<?php

return [
    'src/Package.php' => 'src/:uc:package.php',
    'config/package.php' => 'config/:lc:package.php',
    'src/Facades/Package.php' => 'src/Facades/:uc:package.php',
    'src/PackageServiceProvider.php' => 'src/:uc:packageServiceProvider.php',
];